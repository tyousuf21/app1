package javaapplication28;


public class JavaApplication28 {


    public static void main(String[] args) {
        double number1 = Double.parseDouble (args [ 0 ] );
        double number2 = Double.parseDouble (args [ 1 ] );
        doConversion (number1);
        // Let us compute the fahrenheit equivalent of the second number
        double fahr2 = (number2 * 9 / 5) + 32;
        System.out.println ("Converting " + number2 + " to fahrenheit gives " + fahr2);
    }

    private static void doConversion(double number1) {
        // Let us compute the fahrenheit equivalent of the first number
        double fahr1 = (number1 * 9 / 5) + 32;
        System.out.println ("Converting " + number1 + " to fahrenheit gives " + fahr1);
    }
    
}




